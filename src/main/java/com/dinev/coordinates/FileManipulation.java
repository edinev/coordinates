package com.dinev.coordinates;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.*;

public class FileManipulation
{
    /**
     *Export PointGroup List to JSON file
     * @param groupName PointGroup object name
     * @param fileName path and name of the file as String
     * @throws IOException
     */
    public static void exportPointGroupToJson(PointGroup groupName, String fileName) throws IOException
    {
        groupName.deselectAllPoints();
        String str = new Gson().toJson(groupName);
        BufferedWriter writeTo = new BufferedWriter(new FileWriter(fileName + ".json"));
        writeTo.write(str);

        writeTo.close();

        System.out.println("\n Group saved to " + fileName + ".json " + "successfully");
    }

    public static void importPointGroupFromJson()
    {

    }

    /**
     * Export PointGroup List to TXT file
     * @param groupName
     * @param fileName
     */
    public static void exportPointGroupToTxt(PointGroup groupName, String fileName) throws FileNotFoundException, IOException
    {
//        FileOutputStream fileOutputStream = new FileOutputStream(fileName + "txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName));
        objectOutputStream.writeObject(groupName);
    }

    /**
     * Import TXT file into PointGroup object
     * @param fileName name of the txt file to be imported into PointGroup
     * @return
     * @throws ClassNotFoundException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static PointGroup importTxtToPointGroup(String fileName) throws ClassNotFoundException, FileNotFoundException, IOException
    {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName));
        PointGroup imported;
        return imported = (PointGroup) objectInputStream.readObject();

    }
}
