package com.dinev.coordinates;


import java.util.Scanner;

public class BlastPoint extends DrillPoint
{
    private int explosiveAmount;
    private double stem;
    private DetonatorTiming delayDetonator;
    private ConnectorTiming delayConnector;

//    Кой по ред на взривяване е сондажът.
    private int delayTime;


    public BlastPoint(int number, double northing, double easting, double elevation, double length, int diameter,
                      double stem, DetonatorTiming detonator, ConnectorTiming connector)
    {
        super(number, northing, easting, elevation, length, diameter);

//        The amount of explosive is calculated on the basis of the stem
        double explosiveLength = length - stem;
        int kgExplosivePerMeter = 9;
        this.explosiveAmount = (int) (explosiveLength * kgExplosivePerMeter);
        this.stem = stem;
        this.delayDetonator = detonator;
        this.delayConnector = connector;
        this.delayTime = -1;
    }

    @Override
    public String toString()
    {
        return super.toString() +
                "\nExplosive amount: " + this.explosiveAmount + " kg" +
                "\nStem:             " + this.stem + " m" +
                "\nDelay detonator:  " + this.delayDetonator.getDetonator() + " ms" +
                "\nDelay connector:  " + this.delayConnector.getConnector() + " ms";
    }

    public int getExplosiveAmount()
    {
        return explosiveAmount;
    }

    public void setExplosiveAmount(int explosiveAmount)
    {
        this.explosiveAmount = explosiveAmount;
    }

    public double getStem()
    {
        return stem;
    }

    public void setStem(double stem)
    {
        this.stem = stem;
    }

    public ConnectorTiming getDelayConnector()
    {
        return delayConnector;
    }

    public DetonatorTiming getDelayDetonator()
    {
        return delayDetonator;
    }

    public int getDelayTime()
    {
        return delayTime;
    }

    public void setDelayTime(int delayTime)
    {
        this.delayTime = delayTime;
    }

    @Override
    public String displayPoint()
    {
        return super.displayPoint() + "; Q: " + getExplosiveAmount() + "; S: " + getStem() + "; Det; " + getDelayConnector() +
                "; Con: " + getDelayDetonator();
    }

    @Override
    public void edit()
    {
        super.edit();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Stem" + getStem() + " enter new value: ");
        double newDouble = scanner.nextInt();
        if (newDouble >= 0) {
            setStem(newDouble);

            double explosiveLength = getLength() - stem;
            int kgExplosivePerMeter = 9;
            setExplosiveAmount((int) (explosiveLength * kgExplosivePerMeter));
        }
    }

    public void delayN()
    {

    }
}

enum ConnectorTiming
{
    MS0(0),
    MS17(17),
    MS25(25),
    MS42(42),
    MS67(67),
    MS109(109);

    private int connector;

    ConnectorTiming(int levelCode)
    {
        this.connector = levelCode;
    }

    public int getConnector()
    {
        return this.connector;
    }
}

enum DetonatorTiming
{
    LP0(0),
    LP475(475),
    LP500(500),
    LP1000(1000);

    private int detonator;

    DetonatorTiming(int levelCode)
    {
        this.detonator = levelCode;
    }

    public int getDetonator()
    {
        return this.detonator;
    }



}

