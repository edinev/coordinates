package com.dinev.coordinates;
import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        PointGroup<BlastPoint> blastField1 = new PointGroup<BlastPoint>("Blast field 1");
        blastField1.addPoint(new BlastPoint(1, 111.1, 111.2, 750.3, 14.2, 130,2.5, DetonatorTiming.LP475, ConnectorTiming.MS25));
        blastField1.addPoint(new BlastPoint(2, 123.2, 123.2, 750.3, 14.2, 130,2.5, DetonatorTiming.LP475, ConnectorTiming.MS25));
        blastField1.addPoint(new BlastPoint(3, 123.2, 123.2, 750.3, 14.2,130,2.5, DetonatorTiming.LP475, ConnectorTiming.MS67));
        blastField1.addPoint(new BlastPoint(4, 123.2, 123.2, 750.3, 14.2,130,2.5, DetonatorTiming.LP475, ConnectorTiming.MS25));
        blastField1.addPoint(new BlastPoint(5, 38491.32, 32842.23, 770.43, 14.4, 130, 2.5, DetonatorTiming.LP500, ConnectorTiming.MS42));
        blastField1.displayPointGroup();
        blastField1.deletePoint(5);
        blastField1.displayPointGroup();

        PointGroup<DrillPoint> drillField1 = new PointGroup<DrillPoint>("Drill field 1");
        drillField1.addPoint(new DrillPoint(1, 11111.1, 2321.3, 500.34,17.5, 130));
        drillField1.addPoint(new DrillPoint(2, 44321.3, 2321.3, 500.34,17.5, 130));
        drillField1.addPoint(new DrillPoint(3, 44321.3, 2321.3, 500.34,17.5, 130));
        drillField1.addPoint(new DrillPoint(4, 44321.3, 2321.3, 500.34,17.5, 130));
        drillField1.addPoint(new DrillPoint(5, 44321.3, 2321.3, 500.34,17.5, 130));


        blastField1.selectPoint(1);
        blastField1.selectPoint(2);
        blastField1.selectPoint(3);
        blastField1.selectPoint(4);
        blastField1.deselectAllPoints();
        blastField1.selectAllPoints();

//        export Blast Field 1 group to JSON
        FileManipulation.exportPointGroupToJson(blastField1, "newGroup");

//      export Blast Field 1 to TXT
        FileManipulation.exportPointGroupToTxt(blastField1, "newGroupTxt.txt");

//        import Blast Field 1 group from TXT
        PointGroup<BlastPoint> blastFieldImport;
        blastFieldImport = FileManipulation.importTxtToPointGroup("newGroupTxt.txt");
        blastFieldImport.displayPoint(3);

        drillField1.setDelayTimePoint(1);
        blastField1.setDelayTimePoint(1);



    }
}
