package com.dinev.coordinates;

import java.util.Scanner;

public class DrillPoint extends BasicPoint
{
    private double length;
    private int diameter;
//    private static int totalNumberOfDrillPoints;

    public DrillPoint(int number, double northing, double easting, double elevation, double length, int diameter)
    {
        super(number, northing, easting, elevation);
        this.length = length;
        this.diameter = diameter;
//        totalNumberOfDrillPoints++;
    }

    @Override
    public String toString()
    {
        return super.toString() +
                "\nLength:           " + this.length + " m" +
                "\nDiameter:         " + this.diameter + " mm";
    }

    public double getLength()
    {
        return length;
    }

    public void setLength(double length)
    {
        this.length = length;
    }

    public int getDiameter()
    {
        return diameter;
    }

    public void setDiameter(int diameter)
    {
        this.diameter = diameter;
    }

    public int compareTo(BasicPoint o)
    {
        return 0;
    }

    @Override
    public void edit()
    {
        super.edit();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Length " + getLength() + " enter new value: ");
        double newValue = scanner.nextDouble();
        if (newValue >= 0) {
            setLength(newValue);
        }

        System.out.print("Diameter " + getDiameter() + " enter new value: ");
        int newInt = scanner.nextInt();
        if (newInt >= 0) {
            setDiameter(newInt);
        }


    }

    @Override
    public String displayPoint()
    {
        return super.displayPoint() + "; D: " + getDiameter() + "; L: " + getLength();
    }
}
