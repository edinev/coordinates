package com.dinev.coordinates;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class PointGroup<T extends BasicPoint> implements Serializable
{
    private String name;
    private int numberOfPoints = 0;
    private ArrayList<T> members = new ArrayList<T>();

    public PointGroup(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public int getNumberOfPoints()
    {
        return numberOfPoints;
    }

    /**
     * Add point to the group
     * @param point
     * @return
     */
    public boolean addPoint(T point)
    {
        if (findPoint(point.getNumber()) != null) {
            System.out.println(point.getNumber() + " already in the group " + this.name);
            return false;
        } else {
            members.add(point);
            System.out.println(point.getNumber() + " added in " + this.name + " group");
            numberOfPoints++;
            return true;
        }
    }

    /**
     * Delete point from the group if found.
     * @param pointNumber point to be deleted from the point group
     */
    public void deletePoint(int pointNumber)
    {

        if (findPoint(pointNumber) != null) {
            members.remove(pointNumber-1);
            System.out.println("\nPoint " + pointNumber + " has been removed");
            numberOfPoints--;
        } else {
            System.out.println("\nPoint " + pointNumber + " does not exist.");
        }

    }

    public void displayPointGroup()
    {
        System.out.println("Point group name: " + getName());
        System.out.println("Total number of points: " + getNumberOfPoints());
        System.out.println("Point group type: " + members.getClass().getSimpleName());
        for (T member : members) {
            System.out.println(member.displayPoint());
        }
    }

//    TODO: Трябва да се направи метод, който задава поредноста на взривяване на сондажите в група Blast
    public void setDelayTimePoint(int pointNumber)
    {
        BlastPoint blastPoint;
        T point = findPoint(pointNumber);
        if (point != null) {

            if (point instanceof BlastPoint) {
                blastPoint = (BlastPoint) point;
                if (blastPoint.getDelayTime() == -1) {
                    System.out.println("Blast hole is not connected. Connector type: " + blastPoint.getDelayConnector());
                }
            } else {
                System.out.println("Point  " + point.getNumber() +" from  point group " + this.name + " is not blast point");
            }
        }
    }

    public void displayPoint(int pointNumber)
    {
        System.out.println("\n---=======---");
        BasicPoint checkedPoint = findPoint(pointNumber);
        System.out.println("Point group name: " + this.name);
        System.out.println(checkedPoint);
        System.out.println("---=======---\n");

        return;
    }

//    TODO: method for editing points

    public void editPoint(int pointNumber)
    {
        if (findPoint(pointNumber) != null) {
            findPoint(pointNumber).edit();
        }
    }



    public void displayGroup()
    {
        if (numberOfPoints == 0) {
            System.out.println("Group is empty.");
        } else {
            System.out.println(
                            "\n---=======---" +
                            "\nPoint group:  " + this.name +
                            "\nTotal points: " + this.numberOfPoints +
                            "\n---=======---\n");
        }
    }

    /**
     * TODO: Method for clicking on point in a group.
     * I am going to use the example reserve() in Collections List Method Lesson (N 154.)
     * @param pointNumber click on point if found.
     * @return return true and use setSelect() method if point is found. Return false if there is no such point in the group.
     */
    public boolean selectPoint(int pointNumber)
    {
        BasicPoint point = findPoint(pointNumber);
        if (point != null) {
            if (!point.getSelect()) {
                point.setSelect();
                System.out.println("Point " + point.getNumber() + " is selected");
            } else if (point.getSelect()) {
                point.setSelect();
                System.out.println("Point " + point.getNumber() + " is  deselected.");
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Select all the elements in the point group
     */
    public void selectAllPoints()
    {
        for (int i = 0; i < members.size(); i++) {
            if (!members.get(i).getSelect()) {
                members.get(i).setSelect();
            }
        }
        System.out.println("All points in " + this.name + " group are selected.");
    }

    /**
     * Deselect all elements in the point group.
     */
    public void deselectAllPoints()
    {
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getSelect()) {
                members.get(i).setSelect();
            }
        }
    }

    /**
     * Returns the searched point in the group if found.
     * @param pointNumber point number in the group to be found.
     * @return return point element if found.
     */
    private T findPoint(int pointNumber)
    {
        for (T checkedPoint : this.members) {
            if (checkedPoint.getNumber() == pointNumber) {
                return checkedPoint;
            }
        }
        return null;
    }

    private BlastPoint findBlastPoint(int pointNumber)
    {
        for (T checkedPoint : this.members) {
            if (checkedPoint.getNumber() == pointNumber) {
                if (checkedPoint instanceof BlastPoint) {
                    System.out.println("YES YES YES");
                    BlastPoint p;
                    return p = (BlastPoint) checkedPoint;
                }
            }
        }
        System.out.println("Not a blast point");
        return null;
    }

    public int size()
    {
        return members.size();
    }



}
