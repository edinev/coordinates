package com.dinev.coordinates;

import java.io.Serializable;
import java.util.Scanner;

public abstract class BasicPoint implements Serializable, Comparable<BasicPoint>
{
    private final int number;
    private double northing;
    private double easting;
    private double elevation;

    private boolean select = false;

    public BasicPoint(int number, double northing, double easting, double elevation)
    {
        this.number = number;
        this.northing = northing;
        this.easting = easting;
        this.elevation = elevation;
    }

    public boolean setSelect()
    {
        if (!this.select) {
            this.select = true;
            return true;
        } else {
            this.select = false;
            return false;
        }
    }

    public boolean getSelect()
    {
        return select;
    }

    public int getNumber()
    {
        return number;
    }

    @Override
    public String toString()
    {
        String selectMark;
        if (select) {
            selectMark = "[x]";
        } else {
            selectMark = "[ ]";
        }

        return
                        "Number:           " + number + " " + selectMark +
                        "\nNorthing:         " + northing +
                        "\nEasting:          " + easting +
                        "\nElevation:        " + elevation + " m";
    }


    public double getNorthing()
    {
        return northing;
    }

    public void setNorthing(double northing)
    {
        this.northing = northing;
    }

    public double getEasting()
    {
        return easting;
    }

    public void setEasting(double easting)
    {
        this.easting = easting;
    }

    public double getElevation()
    {
        return elevation;
    }

    public void setElevation(double elevation)
    {
        this.elevation = elevation;
    }

    public void edit()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Northing " + getNorthing() + " enter new value: ");
        double newValue = scanner.nextDouble();
        if (newValue >= 0) {
            setNorthing(newValue);
        }

        System.out.print("Easting " + getEasting() + " enter new value: ");
        newValue = scanner.nextDouble();
        if (newValue >= 0) {
            setEasting(newValue);
        }

        System.out.print("Elevation " + getElevation() + " enter new value: ");
        newValue = scanner.nextDouble();
        if (newValue >= 0) {
            setElevation(newValue);
        }
    }

    //    TODO:
    public String displayPoint()
    {
        return "P: " + getNumber() + "; N: " + getNorthing()+ "; E: " + getEasting()+ "; E: " + getElevation();
    }




}
